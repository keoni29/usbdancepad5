/*
 * dance_pad.c
 *
 *  Created on: Mar 30, 2018
 *      Author: Koen van Vliet
 */

//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------
#include <stdbool.h>
#include "usbd_hid.h"

//-----------------------------------------------------------------------------
// External variables
//-----------------------------------------------------------------------------
extern USBD_HandleTypeDef hUsbDeviceFS;

//-----------------------------------------------------------------------------
// Private variables
//-----------------------------------------------------------------------------
static const uint8_t kMaskUpLeft = (1<<0);
static const uint8_t kMaskUpRight = (1<<1);
static const uint8_t kMaskCenter = (1<<2);
static const uint8_t kMaskDownLeft = (1<<3);
static const uint8_t kMaskDownRight = (1<<4);

//-----------------------------------------------------------------------------
// Prototypes
//-----------------------------------------------------------------------------
static bool SwitchUpLeftPressed(void);
static bool SwitchUpRightPressed(void);
static bool SwitchCenterPressed(void);
static bool SwitchDownLeftPressed(void);
static bool SwitchDownRightPressed(void);

//-----------------------------------------------------------------------------
// Public functions
//-----------------------------------------------------------------------------
void DancePadTask(void)
{
  uint8_t buttons;
  uint32_t pollIntervalMs;
  pollIntervalMs = USBD_HID_GetPollingInterval(&hUsbDeviceFS);


  while (1)
  {
    buttons = 0;

    if (SwitchUpLeftPressed())
    {
      buttons |= kMaskUpLeft;
    }

    if (SwitchUpRightPressed())
    {
      buttons |= kMaskUpRight;
    }

    if (SwitchCenterPressed())
    {
      buttons |= kMaskCenter;
    }

    if (SwitchDownLeftPressed())
    {
      buttons |= kMaskDownLeft;
    }

    if (SwitchDownRightPressed())
    {
      buttons |= kMaskDownRight;
    }

    USBD_HID_SendReport(&hUsbDeviceFS, &buttons, 1);
    HAL_Delay(pollIntervalMs);
  }
}

//-----------------------------------------------------------------------------
// Private functions
//-----------------------------------------------------------------------------

// TODO debounce switches.
static bool SwitchUpLeftPressed(void)
{
  return HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_12) ? false : true;
}

static bool SwitchUpRightPressed(void)
{
  return HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13) ? false : true;
}

static bool SwitchCenterPressed(void)
{
  return HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14) ? false : true;
}

static bool SwitchDownLeftPressed(void)
{
  return HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15) ? false : true;
}

static bool SwitchDownRightPressed(void)
{
  return HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_8) ? false : true;
}

