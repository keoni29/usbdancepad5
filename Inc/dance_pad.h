/*
 * dance_pad.h
 *
 *  Created on: Mar 30, 2018
 *      Author: Koen van Vliet
 */

#ifndef DANCE_PAD_H_
#define DANCE_PAD_H_

/**
 * @brief Read switches and generate USB HID Report.
 */
void DancePadTask(void);


#endif /* DANCE_PAD_H_ */
